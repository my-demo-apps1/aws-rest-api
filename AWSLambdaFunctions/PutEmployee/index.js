const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

exports.handler = function(event, context, callback) {
 const { Department, Firstname, Lastname, Title } = event;
 const { awsRequestId } = context;
 const SECONDS_IN_AN_HOUR = 60 * 60;
 const secondsSinceEpoch = Math.round(Date.now() / 1000);
 const expirationTime = secondsSinceEpoch + 24 * SECONDS_IN_AN_HOUR;

 const params = {
  Item: {
   EmployeeId: {
    S: awsRequestId
   },
   Department: {
    S: Department
   },
   Firstname: {
    S: Firstname
   },
   Lastname: {
    S: Lastname
   },
   Title: {
    S: Title
   },
   TTL: {
    N: `${expirationTime}`
   }
  },
  ReturnConsumedCapacity: "TOTAL",
  TableName: "Employees"
 };


 dynamoDB.putItem(params, function(err, data) {
  if (err) {
   // an error occurred
   console.log(err, err.stack);
   callback(err, null);
  }
  else { 
   // successful response
   const body = {
    "message": "Update Success",
    "data": data
   }
   const response = {
    "statusCode": 200,
    "body": body
   }
   callback(null, response);
  }
 });

}