const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

exports.handler = function(event, context, callback) {
    const { EmployeeId, Department, Firstname, Lastname, Title } = event;
   
    // Update the item, unconditionally,
    var params = {
        TableName: "Employees",
        Key: {
            "EmployeeId": { S: EmployeeId }
        },
        UpdateExpression: "set Department = :d, Firstname=:fn, Lastname=:ln, Title=:t",
        ExpressionAttributeValues: {
            ":d": { S: Department },
            ":fn": { S: Firstname },
            ":ln": { S: Lastname },
            ":t": { S: Title }
        },
        ReturnValues: "UPDATED_NEW"
    };

    dynamoDB.updateItem(params, function(err, data) {
        if (err) {
            // an error occurred
            console.log(err, err.stack);
            callback(err, null);
        }
        else {
            const body = {
                "message": "Update Success",
                "data": data
            }
            const response = {
                "statusCode": 200,
                "body": body
            }
            callback(null, response);
        }
    });
};
