const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback) {
    // event: { queryStringParameters: { id: 'H012345' }, }
    const { queryStringParameters } = event;
    const id = queryStringParameters ? queryStringParameters["id"] : null;
    let params = {
            TableName: 'Employees',
            Key: {
                "EmployeeId": id
            },
            ExpressionAttributeNames: {
                "#e": "EmployeeId",
                "#fn": "Firstname",
                "#ln": "Lastname",
                "#t": "Title",
                "#d": "Department",
            },
            ProjectionExpression: "#e, #fn, #ln, #t, #d",
            Limit: 40 //maximum result of 40 items
        };
    if (id) {
        dynamoDB.get(params, function(err, data) {
            if (err) {
                console.log(err);
                callback(err, null);
            }
            else {
                const response = {
                    "statusCode": 200,
                    "body": JSON.stringify(data.Item),
                    "isBase64Encoded": false
                }
                callback(null, response);
            }
        });
    }
    else {

        //Will scan entire table in dynamoDB and return results.
        dynamoDB.scan(params, function(err, data) {
            if (err) {
                console.log(err);
                callback(err, null);
            }
            else {
                const response = {
                    "statusCode": 200,
                    "body": JSON.stringify(data),
                    "isBase64Encoded": false
                }
                callback(null, response);
            }
        });

    }
}