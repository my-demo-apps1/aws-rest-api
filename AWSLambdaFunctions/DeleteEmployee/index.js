const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback) {
    //   event: { queryStringParameters: { id: 'H012345' }, }
    const { queryStringParameters } = event;
    const id = queryStringParameters ? queryStringParameters["id"] : null;
    let params = {
        TableName: 'Employees',
        Key: {
            "EmployeeId": id
        }
    };
    if (id) {
        console.log("Getting the item with params->", params);
        dynamoDB.delete(params, function(err, data) {
            if (err) {
				console.log(err);
                callback(err, null);
            }
            else {
                const body = {
                    message: "Delete Successful"
                }
                const response = {
                    "statusCode": 200
                }
                callback(null, response);
            }
        });
    }
    else {
        const response = {
            "statusCode": 601,
            "statusText": "NO_KEY",
            "body": "No key was provided to perform the delete item operation"
        }
        callback(null, response);
    }
}
